package strategy.model;

public class Ticket {
	private Watcher w;
	private MovieType type; // na jaki rodzaj filmu jest ten bilet (na podstawie wieku!)
	
	
	public Ticket(Watcher w, MovieType type) {
		super();
		this.w = w;
		this.type = type;
	}
	
	public Watcher getW() {
		return w;
	}
	public void setW(Watcher w) {
		this.w = w;
	}
	public MovieType getTicketType() {
		return type;
	}
	public void setType(MovieType type) {
		this.type = type;
	}
	
	
}
