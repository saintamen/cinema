package strategy.model;

public enum MovieType {
	ADULT(2), UNDERAGE(1), KID(0);
	int type;
	private MovieType(int type) {
		this.type = type;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
}
