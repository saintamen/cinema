package strategy;

import strategy.model.MovieType;
import strategy.model.Ticket;
import strategy.model.Watcher;

public class TicketFactory {

	public static Ticket createKidTicket(Watcher w) {
		return new Ticket(w, MovieType.KID);
	}

	public static Ticket createUnderageTicket(Watcher w) {
		return new Ticket(w, MovieType.UNDERAGE);
	}

	public static Ticket createAdultTicket(Watcher w) {
		return new Ticket(w, MovieType.ADULT);
	}

}
