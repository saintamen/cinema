package strategy;
import strategy.model.MovieType;
import strategy.model.Watcher;
import strategy.policies.LoudWatcherCheckPolicy;

public class Main {

	public static void main(String[] args) {
		Cinema c = new Cinema (MovieType.ADULT, 3, new LoudWatcherCheckPolicy());

		c.addWatcher(new Watcher("adam", 5, true));
		c.addWatcher(new Watcher("burek", 15, false));
		c.addWatcher(new Watcher("waldek", 25, false));
		c.addWatcher(new Watcher("mikołaj", 30, true));
		c.addWatcher(new Watcher("jan", 10, false));

		c.startMovie();
		c.checkWatchers();
	}

}
