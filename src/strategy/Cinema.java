package strategy;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import strategy.model.MovieType;
import strategy.model.Ticket;
import strategy.model.Watcher;
import strategy.policies.IWatcherPolicy;

public class Cinema {
	private MovieType currentMovie; // aktualnie wyświetlany film
	private int seatsNumber;
	private IWatcherPolicy policy;
	private List<Ticket> watchers = new LinkedList<>();
	private boolean isMovieStarted =false;
	
	public Cinema(MovieType currentMovie, int seatsNumber, IWatcherPolicy policy) {
		super();
		this.currentMovie = currentMovie;
		this.seatsNumber = seatsNumber;
		this.policy = policy;
	}

	public void addWatcher(Watcher w){
		if(isMovieStarted){
			System.out.println("Nie można dodać oglądających, film rozpoczęty");
			return;
		}
		if(seatsNumber > watchers.size()){
			Ticket t = null;
			if(w.getAge() < 3){
				t = TicketFactory.createKidTicket(w);
			}else if(w.getAge() < 21 ){
				t = TicketFactory.createUnderageTicket(w);
			}else {
				t = TicketFactory.createAdultTicket(w);
			}
			watchers.add(t);
		}else{
			System.out.println("Brak miejsc");
		}
	}
	
	public void startMovie(){
		isMovieStarted = true;
	}
	
	public void checkWatchers(){
		Iterator<Ticket> it = watchers.iterator();
		while(it.hasNext()){
			Ticket ticketChecked = it.next();
			if(policy.checkWatcher(ticketChecked, currentMovie)){
				System.out.println("Wyrzucam: " + ticketChecked.getW().getName());
				it.remove();
			}
		}
	}
	
	public void stopMovie(){
		isMovieStarted = false;
		watchers.clear();
	}
}
