package strategy.policies;
import java.util.Random;

import strategy.model.MovieType;
import strategy.model.Ticket;

public class LoudWatcherCheckPolicy implements IWatcherPolicy {
	private Random r = new Random();

	@Override
	public boolean checkWatcher(Ticket ticketChecked, MovieType currentMovie) {
		int los = r.nextInt(100);
		if (los < 10) {
			return true; // wyrzucam dla <10
		}
		return false;
	}
}
