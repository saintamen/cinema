package strategy.policies;
import strategy.model.MovieType;
import strategy.model.Ticket;

public interface IWatcherPolicy {
	boolean checkWatcher(Ticket ticketChecked, MovieType currentMovie);
}
