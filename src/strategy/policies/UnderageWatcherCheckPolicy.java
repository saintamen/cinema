package strategy.policies;
import strategy.model.MovieType;
import strategy.model.Ticket;

public class UnderageWatcherCheckPolicy implements IWatcherPolicy{
	@Override
	public boolean checkWatcher(Ticket ticketChecked, MovieType currentMovie) {
		if(ticketChecked.getTicketType().getType() < currentMovie.getType()){
			return true;	// wyrzucam
		}
		return false;	// nie wyrzucam
	}
}
