package strategy.policies;
import strategy.model.MovieType;
import strategy.model.Ticket;

public class NoWatcherCheckPolicy implements IWatcherPolicy{

	public boolean checkWatcher(Ticket ticketChecked, MovieType currentMovie) {
		return false;
	}
}
